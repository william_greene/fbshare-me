<?php
/*
Processes nginx logs into a format that Hive can handle. See fbsharelogs.txt
*/


// total lines to process
$maxLines = 100000000;
$linesPerFile = 1000000;
//$maxLines = 100;

$fh = fopen('./nginx_access.log','r');
$i = 0;
$outfile = 0;
$of = null;
while ( ($line = fgetcsv($fh,4096," ",'"')) && $i < $maxLines)
{
        if ( ($i % $linesPerFile) == 0) {
                if (!is_null($of)) fclose($of);
                $outfile = "/mnt/logs/ma-$i.tsv";
                $of = fopen($outfile,'a+');
        }
        $pathParts = parse_url($line[5]);
        parse_str($pathParts['query'],$queryParts);
        $originalUrl = parse_url($queryParts['url']);
        $referrer = parse_url($line[9]);
        $timeString = substr($line[3],1);
        list($dateString,$hour,$min,$sec) = explode(':',$timeString);
        list($day,$month,$year) = explode('/',$dateString);
        $timestamp = strtotime("$day-$month-$year $hour:$min:$sec");
//echo "$day-$month-$year $hour:$min:$sec\n";
//echo $timestamp."\n";
        $date = strftime('%Y-%m-%d %H:%M:%S',$timestamp);
echo $date."\n";
//exit;
        $fields = array(
                'accessed' => $date,
                'path' => $line[5],
                'http_code' => $line[6],
                'referrer' => $line[9],
                'referrer_domain' => $referrer['host'],
				                'referrer_path' => $referrer['path'],
				                'user_agent' => $line[10],
				                'ip' => $line[11],
				                'resource_requested' => $pathParts['path'],
				                'button_size' => $queryParts['size'],
				                'original_url' => $queryParts['url'],
				                'original_url_domain' => $originalUrl['host'],
				                'original_url_path' => $originalUrl['path']
				        );

				        $originalUrl = null;
				        $referrer = null;
				        $originalUrl = null;
				        $outline = implode("\t",$fields)."\n";
				        fwrite($of,$outline);
				        $i++;
				}