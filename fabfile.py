from fabric.api import env, task, abort
from fabric.decorators import hosts, roles

import awesm_deploy.git as git
from awesm_deploy.deploy import cluster, update_conf_json, show_conf, show_conf_data

env.app_name = 'fbshare'
env.deploy_dir = '/var/www/fbshare/'
env.repo_url = 'git@github.com:awesm/fbshare-me.git'
env.services = [ 'nginx','memcached','php-fastcgi' ]

@task
def deploy(obj=None):
	"""Deploy to a host specified as a command line argument"""
	git.deploy(obj, restart_svc=True)
