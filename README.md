# Building fbshare.me

## New instance
Follow these [instructions](http://wiki.thesnowballfactory.com/New_instance_deploy) to bring up a new instance

## Puppet classes
Add the following puppet classes

        classes:
            awesm: null
            fbshare: null

Run puppetd a few times on the new instance to install the required packages.

# Testing
Modify /etc/hosts and add the following.  The IP is the external address of haproxy-fbshare-STAGE

        54.242.92.41 www.fbshare.me
        54.242.92.41 widgets.fbshare.me

On the haproxy, edit **/etc/haproxy/haproxy.cfg** and add the followings to the **backend fbshare** section

        server fbshare-3a 10.240.91.15:8080 check inter 5s fastinter 2s

Go to www.fbshare.me/index.html, widgets.fbshare.me/files/fbshare.php on a browser, check out /var/log/nginx on the new instance for errors
