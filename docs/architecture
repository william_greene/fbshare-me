fbshare.me
==========

Setup instructions are in README. This is the whys and hows.

What is fbshare.me for?
-----------------------

Before we consolidated all our share buttons, we wanted a Facebook share button 
that counted awe.sm clicks as well as ordinary shares. Since then, Facebook has 
clamped down further and further on their API, and now this part of the 
functionality is almost totally absent (it still tries to get counts but is 
rate-limited to a degree that prevents most calls working).

It still gets counts of awe.sm-powered shares if you put in an API key.

However, it is still extremely popular with mostly Spanish-speaking blogs, so we keep
it running in case we want to do something with it some day.

What does it do?
----------------

The flow is simple:

1. User A visits bob.com/foo
2. The page contains an iframe to widgets.fbshare.me/files/fbshare.php
OR
2a. The page contains a call to widgets.fbshare.em/files/widgets.js, which in turn creates the iframe
3. Our button looks in memcache (shared between the two instances and resides on fbshare-new-2) for current stats for the button
	- these stats come from both facebook and, if an API key is present, awe.sm
	- if the stats aren't present, we make API calls to fetch them
4. We render a button
5. User A clicks the button
6. Button opens a share dialog via awe.sm's /share endpoint, creating a redirect link in the process
OR
6a. If no API key is present, just an ordinary share dialog.

