# Decpreated

fbshare used to run behind haproxy (haproxy-fbshare).  The two instances running behind the haproxies listen on port 8080.  The outside world connects to the haproxy using www.fbshare.me through AWS elastic ip.  Below are puppet classes for setting up the haproxy

    # haproxy-fbshare puppet classes
    classes:
      awesm:
        check_puppet_agent: false
      haproxy::fbshare: null

The security group for the haproxy is **haproxy-ssl**
