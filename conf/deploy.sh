#!/bin/bash
cd /var/www
TODAY=`date +%Y-%m-%d-%H-%M-%S`
DEPLOYDIR="/var/www/deploy/fbshare-$TODAY"
cp -r /home/ubuntu/fbshare-me $DEPLOYDIR
rm current
ln -s $DEPLOYDIR current
