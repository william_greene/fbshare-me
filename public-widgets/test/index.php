<h1>Testing PHP only</h1>
<?php

//$url = "http://fbshareme-1/files/fbshare.php?size=large&url=http://www.fbshare.me/&title=Sharecount%20Button%20for%20Facebook&google_analytics=true

//$baseUrl = "http://10.0.1.107/files/fbshare.php?";
$baseUrl = "http://fbshare.me/files/fbshare.php?";

$tests = array(
  'small' => array(
	  'size' => 'small',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'google_analytics' => null,
	  'awesm_api_key' => null,
	  'badge_text' => null,
	  'badge_color' => null
  ),
  'large' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'google_analytics' => null,
	  'awesm_api_key' => null,
	  'badge_text' => null,
	  'badge_color' => null
  ),
  'noapi-ga' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'google_analytics' => true,
	  'awesm_api_key' => null,
	  'badge_text' => null,
	  'badge_color' => null
  ),
  'api-ga' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'google_analytics' => true,
	  'awesm_api_key' => 'bb9a7e96e7f43bec1daf9e3944d5cf9b3290e180a89bc746b7d1f38a4ee508ca',
	  'badge_text' => null,
	  'badge_color' => null
  ),
  'api-demo' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'google_analytics' => true,
	  'awesm_api_key' => '5c8b1a212434c2153c2f2c2f2c765a36140add243bf6eae876345f8fd11045d9',
	  'badge_text' => null,
	  'badge_color' => null
  ),
  'small-ten' => array(
	  'size' => 'small',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 10
  ),
  'small-hundred' => array(
	  'size' => 'small',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 198
  ),
  'small-thousand-1' => array(
	  'size' => 'small',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 2222
  ),
  'small-thousand-2' => array(
	  'size' => 'small',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 3456
  ),
  'small-thousand-3' => array(
	  'size' => 'small',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 7559
  ),
  'small-thousand-4' => array(
	  'size' => 'small',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 9999
  ),
  'small-ten-thousand' => array(
	  'size' => 'small',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 94422
  ),
  'large-ten' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 10
  ),
  'large-hundred' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 198
  ),
  'large-thousand-1' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 2234
  ),
  'large-thousand-2' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 3456
  ),
  'large-thousand-3' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 7559
  ),
  'large-thousand-4' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 9998
  ),
  'large-ten-thousand-1' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 94422
  ),
  'large-ten-thousand-2' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 10101
  ),
  'large-ten-thousand-3' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 12345
  ),
  'large-ten-thousand-4' => array(
	  'size' => 'large',
	  'url' => 'http://www.fbshare.me/',
	  'title' => "Some title",
	  'test_count' => 77777
  ),
);

foreach($tests as $title => $test)
{
  echo "<h2>$title</h2>";

  if($test['size'] == 'large') $dimensions = 'width="53" height="69"';
  else $dimensions = 'width="80" height="18"';
  echo '<iframe scrolling="no" '.$dimensions.' frameborder="0" allowtransparency="true" src="'.$baseUrl.http_build_query($test).'"></iframe>';

  /*
   * JS version
  echo 'script>';
  echo 'var fbShare = ' . json_encode($test);
  echo '</script>';
  echo '<script src="http://widgets.fbshare.me/files/fbshare.js"></script>';
   */
  echo '<hr>';
}

?>
