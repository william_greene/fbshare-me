<?php
// prod
define('MEMCACHE_HOST','127.0.0.1'); // localhost
define('MEMCACHE_TIME_SECS','600'); // 10 minutes
define('AWESM_API_TIMEOUT_SECS',10);
define('FACEBOOK_API_TIMEOUT_SECS',1);

// test
//define('IS_DEBUG',true);
//define('MEMCACHE_HOST','127.0.0.1');
//define('MEMCACHE_TIME_SECS','300');
//define('AWESM_API_TIMEOUT_SECS',10);
//define('FACEBOOK_API_TIMEOUT_SECS',1);
//define('MEMCACHE_HOST','not.in.any.way.a.memcache.host');
//define('FB_SHARE_API_BASE', "http://not.the.facebook.api");
//define('AWESM_API_BASE', "http://not.so.awe.sm");

if(!class_exists('Memcache')) {
    class Memcache {
        
        function connect() { return true; }
        function getVersion() { return 1; }
        function get($key) { return false; }
        function set($key,$value) { return true; }

    }
}

// these allow us to override for test cases
if (!defined('AWESM_API_BASE')) define('AWESM_API_BASE', "http://create.awe.sm/url.json?version=1&share_type=facebook-share&original_url=");
if (!defined('FB_SHARE_API_BASE')) define('FB_SHARE_API_BASE', "https://api.facebook.com/method/fql.query?format=json&query=SELECT+share_count,+like_count,+comment_count,+total_count+FROM+link_stat+WHERE+%0Aurl+%3D+");
if (!defined('DEBUG')) define('DEBUG_MAXCOUNT',false);

// get params
$urlToShare = @$_GET['url'];
$pageTitle = @$_GET['title'];
$buttonParam = @$_GET['size'];
$apiKey = @$_GET['awesm_api_key'];
$googleAnalyticsParam = @$_GET['google_analytics'];
$badgeBackParam = @$_GET['badge_color'];
$badgeTextParam = @$_GET['badge_text'];

// XSS protection: only known strings
switch($buttonParam) {
    case 'small':
    case 'large':
        $buttonSize = $buttonParam;
        break;
    default:
        $buttonSize = 'small';
}

// Validation. Analytics param should be boolean.
if ($googleAnalyticsParam === true || $googleAnalyticsParam === 'true') {
    $googleAnalytics = true;
} elseif ($googleAnalyticsParam === false || $googleAnalyticsParam === 'false') {
    $googleAnalytics = false;
} else {
    $googleAnalytics = false;
}

// yet more XSS: we echo the API key, so make sure it's not got any nasties
if (!empty($apiKey) && !eregi("[a-zA-Z0-9]",$apiKey)) {
    ?>
    Invalid API key provided. Stopping for security reasons.
    <?php
    exit;
}
// get the parent awesm id from the referrer if present
$referringUrl = $_SERVER['HTTP_REFERER'];
$referringUrlParts = parse_url($referringUrl);
parse_str($referringUrlParts['query'],$referringUrlArgs);
$parentAwesm = @$referringUrlArgs['awesm'];

// even more XSS: we echo the awesm parent parameter, so make sure it's not got any nasties
if (!empty($parentAwesm) && !eregi("[a-zA-Z0-9\.\-_]",$parentAwesm)) {
    $parentAwesm='';
}

// still more XSS: colors
$badgeBack = validateColor($badgeBackParam,'#3b5998');
$badgeText = validateColor($badgeTextParam,'#fff');

// deal with apiKey vs googleAnalyticsParam
$bUseAwesm = true;
if (empty($apiKey)) {
	if ($googleAnalytics) {
		//if no API Key is specified AND the Google Analytics parameter setting is true, use the fbshare.me API Key with GA params on 
		$apiKey = '62bdeef0fbebb3d97073db57f2554d9d58d93ebed9175c83ab757109e1634909';
	} else {
		//if no API Key is specified, don't use awe.sm
		$bUseAwesm = false;
	}
}

// try 3 times to get stats
$tries = 0;
$stats = false;
$goodStats = false;
while(false && $tries < 3) {
    $stats = getStats($urlToShare, $bUseAwesm);
    if (array_key_exists('total_shares',$stats)) {
        $goodStats = true;
        break; // good result: move on
    }
    if (array_key_exists('wait',$stats)) {
        sleep(1); // it's locked: wait a second and try again
    }
    $tries++;
}

// if no stats, act like there are no shares
if (!$goodStats) {
    $stats = array(
        'total_shares' => 0
    );
}

$totalShares = $stats['total_shares'];

// +testing
if (defined('IS_DEBUG') && !empty($_GET['test_count']))
{
  $totalShares = $_GET['test_count'];
}
// -testing

// get the URL to send to facebook
if($bUseAwesm)
{
  $shareUrl = 'http://create.awe.sm/url/share?version=1&share_type=facebook-post&create_type=fbshare-js-' . $buttonSize;
  $shareUrl .= '&target=' . rawurlencode($urlToShare);
  $shareUrl .= '&destination=' . rawurlencode("http://www.facebook.com/sharer.php?u=AWESM_TARGET&t=$pageTitle");
  $shareUrl .= '&api_key=' . $apiKey;
  $shareUrl .= '&parent_awesm=' . $parentAwesm;
}
else
{
  // just use Facebook sharing
  $shareUrl = "http://www.facebook.com/sharer.php?u=$urlToShare&t=$pageTitle";
}

// CSS
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>FB Share</title>
<style> 

/* common */
	* { margin: 0; padding: 0; }
	
	body {
		font: normal 14px Arial, sans-serif;
		line-height: 15px;
    	color: #000
	}

	a {
		text-decoration:none; 
	}

/* large */	
	.fb_share_large .fb_sharecount {
		line-height: 140%;
    	display: block;
    	padding-top: 1px;
    	background-color: <?php echo $badgeBack ?>;
    	color: <?php echo $badgeText ?>;
    	font-family:"Helvetica",tahoma,verdana,arial,sans-serif;
    	text-align: center;
    	height: 47px;
    	margin-bottom: 2px;
    	-webkit-border-radius: 2px;
    	-moz-border-radius: 2px;
	}
	
	.fb_share_large .fb_sharecount .count {
		display: block;
		margin: auto;
		margin-top: 5px;
		font-size: 25px;
		height: 20px;
		padding-top: 2px;
    	width: 42px;
    	overflow: hidden;
	}
	
	.fb_share_large .fb_sharecount .shares {
		display: block;
		font-size: 11px;
		margin-top: -1px;
	}	

	.fb_share_large .fb_sharecount_zero {
		display: block;
		background: url('http://static.fbshare.me/f_only.png') no-repeat 20px 5px;
    	background-color: <?php echo $badgeBack ?>;
		width: 53px;
		height: 47px;
    	margin-bottom: 2px;
    	-webkit-border-radius: 2px;
    	-moz-border-radius: 2px;
	}

	.fb_share_large .fb_sharecount_zero { /* PNG Alpha IE Win ONLY */
		_background-image: url('http://static.fbshare.me/f_only.gif');
	}
	
	.fb_share_large .fb_sharebutton { 
		display: block;
		padding: 0px 0px 0px 3px;
		width: 48px;
		height: 16px; 
		color: #3B5998;
		font-size:11px;
		font-weight:normal;
		font-family:"lucida grande",tahoma,verdana,arial,sans-serif;
		text-align:left;
		border:1px solid #d8dfea;
		background:url('http://static.fbshare.me/fb_sq_small.png') no-repeat top right;
		background-color: #fff;
	}
	
	.fb_share_large a.fb_sharebutton:hover { 
		color:#fff;
		background-color: #3b5998;
		border: 1px solid #295582;
	}

	.fb_share_large .count.count-1k {
		font-size: 20px;
	}

	.fb_share_large .count.count-10k {
		font-size: 24px;
	}

/* small */
	.fb_share_small {
		float: right;
		background-color: transparent;
	}

	.fb_share_small .fb_sharebutton { 
		display: block;
		padding: 0px 0px 0px 3px;
		width: 48px;
		height: 16px; 
		color: #3B5998;
		font-size:11px;
		font-family:"lucida grande",tahoma,verdana,arial,sans-serif;
		text-align:left;
		border:1px solid #d8dfea;
		background:url('http://static.fbshare.me/fb_sq_small.png') no-repeat top right;
		background-color: #fff;
	}

	.fb_share_small a:hover { 
		color:#fff;
		background-color: #3b5998;
		border-color: #295582;
	}

	.fb_share_small .fb_sharecount {
		position: absolute;
		top: 1px;
		right: 52px;
		font-size: 14px;
		font-weight: bold;
		margin-right: 3px;
		color: #3b5998;
	}
	.fb_share_small .count-1k {
		font-size: 12px;
	}
	.fb_share_small .count-10k {
		font-size: 13px;
	}

</style>
<script>
function share() {
	window.open('<?php echo $shareUrl; ?>','sharer','toolbar=0,status=0,width=626,height=436');
	return false;			
}
</script>
</head>
<body>

<?php
if ($totalShares > 999)
{
  if ($totalShares > 9999)
  {
	error_log("$totalShares is 10k+");
	$shareCountString = round($totalShares/1000,0).'K';
	$shareCountClass = 'count-10k';
  }
  else
  {
	error_log("$totalShares is 1k+");
	$shareCountString = round($totalShares/1000,1).'K';
	$shareCountClass = 'count-1k';
  }
}
else
{
  error_log("$totalShares is sub 1k");
  $shareCountString = $totalShares;
  $shareCountClass = '';
}

?>

<?php if($buttonSize == 'small'): ?>
	<div class="fb_share_small">
		<?php if($totalShares > 0): ?>
			<span class="fb_sharecount <?php echo $shareCountClass; ?>"><?php echo $shareCountString ?></span>
		<?php endif; ?>
<?php elseif ($buttonSize == 'large'): ?>
	<div class="fb_share_large">
		<?php if ($totalShares > 0): ?>
    		<div class="fb_sharecount">
    			<span class="count <?php echo $shareCountClass; ?>"><?php echo $shareCountString; ?></span>
    			<span class="shares">shares</span>
    		</div>
		<?php else: ?>
			<a class="fb_sharecount_zero"
			   href="http://www.facebook.com/share.php?u=<?php echo rawurlencode($urlToShare); ?>"
			   title="Share this on Facebook"
			   class="fb_sharebutton"
			   onclick="return share();"
			   target="_blank"></a>
		<?php endif; ?>
<?php endif; ?>

    <a href="http://www.facebook.com/share.php?u=<?php echo rawurlencode($urlToShare); ?>" 
    	title="Share this on Facebook" 
    	class="fb_sharebutton" 
    	onclick="return share();" 
    	target="_blank">Share</a>
</div>

</body>
</html>
<?php

// functions down here for tidiness

// Get total shares and clicks for a given original_url and share_type=facebook-post
/*
Caching algorithm:
- key = md5 of URL
- look for key
- if key exists
    if key is valid
      if key == -1
        lookup is in progress
        return "retry" to JS
      else
        return value
    else
      delete key
- if key did not exist
    set key to -1
    fetch stats
    if stats valid
      set key to valid value
      return value
    else
      delete key
      return "failed" (JS might retry anyway)
 */
function getStats($url,$bUseAwesm=false)
{
    $key = md5($url);
    
    // check cache
    #$cache = new MyCache(); 
		$cache = false;
		if ($cache) {
        $key = md5($url);
        $cachedResult = $cache->get($key);
        if ($cachedResult) {
            if (isValidEntry($cachedResult)) {
                error_log("Hit $key!");
                return $cachedResult; // hit!
            }
            else if (isLocked($cachedResult)) {
                error_log("Locked $key");
                return array(
                    'wait' => true // locked: tell the JS to retry in a second
                );
            }
            else {
                error_log("ERROR: bogus cache result. Poison?");
                $cachedResult = null; // treat as if no result
            }
        }
        if (empty($cachedResult)) {
            $cache->set($key,-1); // block other lookups while we're fetching
        }
    }

    // no cache hit, no lock: fetch the stats
    $defaultResult = array(
        'total_clicks' => 0,
        'total_shares' => 0
    );
    $returnResult = false;

	if ($bUseAwesm)
	{
	  // try to get the awe.sm stats
	  $apiCall = AWESM_API_BASE . $url;
	  try {
		  // get the data, log error if empty (generally a 500)
			
			// comment out code that creates a share on every page load
			// $result = getData($apiCall,AWESM_API_TIMEOUT_SECS);
		  // if (empty($result)) {
			//  error_log("FATAL: no result from call to $apiCall");
			// }
			$result = array();
	  } catch (Exception $e) {
		  // log a different error if it timed out
		  $result = false;
		  error_log("FATAL: exception " . $e->getMessage() . " (code " . $e->getCode() . ") on awe.sm API call $apiCall");
	  }

	  // parse the awe.sm response if present
	  if($result) {
		  $response = json_decode($result,true);
		  if (empty($response)) {
			  error_log("FATAL: non-JSON response from $apiCall");
		  }
		  // if it's valid, it becomes the base of the response
		  if (isValidEntry($response)) {
			  error_log("Valid response from awe.sm for $apiCall");
			  $returnResult = $response;
		  }
	  }
	}

	// try to get the facebook stats
    $fbCall = FB_SHARE_API_BASE . urlencode("'$url'");
    try {
        // get the data, log empty response
        $fbResult = getData($fbCall,FACEBOOK_API_TIMEOUT_SECS);
        if (empty($fbResult)) {
            error_log("FATAL: no result from call to $fbCall");
        }
    }
    catch (Exception $e) {
        // log a timeout separately
        $fbResult = false;
	    error_log("FATAL: exception " . $e->getMessage() . " (code " . $e->getCode() . ") on Facebook share API call $fbCall");
    }
    
    // parse facebook response if present
	if ($fbResult) {
	    $fbResponse = json_decode($fbResult,true);
	    if (empty($fbResponse)) {
	        error_log("FATAL: non-JSON response from $fbCall");
	    }
	    else if (array_key_exists(0,$fbResponse)) {
	        // it's a valid fb response
	        $fbStats = $fbResponse[0];
	        error_log("Valid response from facebook for $fbCall");
	        if ($returnResult) {
    	        // we already got awe.sm stats, so increment our counts with facebook's
        	    $returnResult['total_shares'] += $fbStats['total_count'];
        	    $returnResult['total_clicks'] += $fbStats['click_count']; // TODO: scrap this
	        }
	        else {
	            // only facebook stats
	            $returnResult = array(
	                'total_shares' => $fbStats['total_count'],
	                'total_clicks' => $fbStats['click_count'] // TODO: scrap this
	            );
	        }
	    }
	}
	
	// if either has returned, we cache the response
	if ($returnResult) {
	    error_log("Set $key");
        $cache->set($key,$returnResult); // valid cache entry
	}
	
	return $returnResult;
}

// validation function for cache entries
function isValidEntry($result) {
    if (!is_array($result)) {
        return false;
    }
    return (array_key_exists('total_shares',$result));
}

// definition of a locked result
function isLocked($result) {
    return ($result == -1);
}

function getData($url,$timeout) {

	error_log("Curling $url");

    $ch = curl_init();
    
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    
    // grab URL and pass it to the browser
    $data = curl_exec($ch);
    
    $error = curl_errno($ch);
    if ($error == 28) { // timeout
        throw new Exception("Timeout after $timeout seconds",28);
    }
    
    // close cURL resource, and free up system resources
    curl_close($ch);
    
    return $data;    
}

// simple wrapper to memcache: prevents errors bubbling up to the user
class MyCache {
    
    /**
     * @var Memcache
     */
    private $cache;

    // connects to memcache
		function __construct() {
        // try connecting to memcache
        $this->cache = new Memcache;
        try {
            $this->cache->connect(MEMCACHE_HOST, 11211);
            $version = $this->cache->getversion();
            if (empty($version)) {
                error_log("Could not connect to host " . MEMCACHE_HOST);
                return false;
            }
        }
        catch (Exception $e) {
            error_log("Exception connecting to memcache: " . $e->getMessage());
            return false;
        }
    }

    function get($key) {
        try {
            return $this->cache->get($key);
        }
        catch (Exception $e) {
            error_log("Exception fetching key $key: " . $e->getMessage());
            return false;
        }
    }
    
    function set($key,$value) {
        try {
            return $this->cache->set($key,$value,false,MEMCACHE_TIME_SECS);
        }
        catch (Exception $e) {
            error_log("Exception setting value $value to key $key: " . $e->getMessage());
            return false;
        }
    }
    
}

function validateColor($colorCode,$default) {
    if (strpos($colorCode,'#') === false) {
        $colorCode = '#'.$colorCode;
    }
    if (preg_match("/^#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/",$colorCode)) {
        return $colorCode;
    }
    else {
        return $default;
    }
}
